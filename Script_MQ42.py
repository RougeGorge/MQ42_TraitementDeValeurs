#! /usr/bin/env python
#################################################################################
#     File Name           :     Script_MQ42.py
#     Created By          :     root
#     Creation Date       :     [2018-04-04 15:29]
#     Last Modified       :     [2018-04-19 15:05]
#     Description         :      
#################################################################################



import csv
import numpy as np
import numpy.core.defchararray as dfc
import _io
import os
import matplotlib.pyplot as plt


#Pour chopper sans prendre la tête les extremas
from scipy.signal import argrelextrema
from math import *


Sources=os.path.abspath("./Sources")
Dest=os.path.abspath("./Resultats")
GraphDir=os.path.abspath("./Resultats/Graphs")
ls = np.array(os.listdir(Sources))

csvFiles=np.array([])

N_File=0
N_Row=0
N_Col=0

csvFileList=np.array([])




def SelectCol(File,Indexs): 
    
    if(type(File)==_io.TextIOWrapper):
        File.seek(0)
        FileContent=File.read().split('\n')
    elif(type(File)==str):
        FileContent=Dico[File].split('\n')
    
    
    n_Row=len(FileContent)
    n_Col=0
    
    for l in FileContent:
        n_Col=max(n_Col,len(l.split(';')))
      
    Unselected=np.delete(range(n_Col),Indexs)
    n_Col=len(Indexs)
    
    NewFileContent=np.array(n_Row*n_Col*[512*" "]).reshape(n_Row,n_Col)
    i=0
    print(NewFileContent.shape)
    
    for l in FileContent:
        NewFileContent[i]=np.delete(l.split(';'),Unselected)
        i=i+1
        
        
    return NewFileContent
      

      
def GetFileList(Patern,Path): #Patern est une regex
    NewFileList=((os.popen("cd "+Path+" && find "+"-regextype sed -regex "+"./"+Patern).read()).replace("./",'')).split('\n')
    NewFileList.remove('')
    return NewFileList
    
def GetParameter(Patern,FileName): #Patern est une regex
    Para=""
    SepChar=" "
    for i in range(len(Patern)):
        if(Patern[i]=='.'):
            
            if(i<=4):
                if(FileName[i]=='1'):
                    if(i==0):
                        Para=Para+"Avec rebond"+SepChar
                    if(i==1):
                        Para=Para+ "avec mousse"+SepChar
                    if(i==2):
                        Para=Para+ "avec jeu"+SepChar
                    if(i==3):
                        Para=Para+ "Embout en élastomère"+SepChar
                    if(i==4):
                        Para=Para+ "Choc sur l'extremité"+SepChar
                    
                elif(FileName[i]=='0'):
                    if(i==0):
                        Para=Para+ "Sans rebond"+SepChar
                    if(i==1):
                        Para=Para+ "sans mousse"+SepChar
                    if(i==2):
                        Para=Para+ "sans jeu"+SepChar
                    if(i==3):
                        Para=Para+ "Embout en alluminium"+SepChar
                    if(i==4):
                        Para=Para+ "Choc au milieu"+SepChar
    return Para
            

def Compare(Patern): #Patern est un regex
    IndexToPlot=[0,1]
    FileToCompare=GetFileList(Patern,Dest)
    #PlotArgs=np.array([])
    if(len(FileToCompare)<=2):
        for FileName in FileToCompare:
            KeyX=list(Dico[FileName].keys())[IndexToPlot[0]]
            KeyY=list(Dico[FileName].keys())[IndexToPlot[1]]
            X=Dico[FileName][KeyX]
            Y=Dico[FileName][KeyY]
            plt.plot(X,Y,label=GetParameter(Patern,FileName))
            plt.title(GetParameter(Patern.replace("0","1").replace(".","0").replace("1","."),FileName))
            plt.legend()
            
        plt.xlabel("Temps (s)")
        plt.ylabel("Accélération (g)")
        plt.savefig(GraphDir+"/"+GetParameter(Patern.replace("0","1").replace(".","0").replace("1",".")
,FileName),format="svg")
        plt.close()
    else:
        print("Impossible to compare more than two files at the same time")

def LocalMax(FileName,Epsilon): #INSTABLE
    KeyX=list(Dico[FileName].keys())[0]
    KeyY=list(Dico[FileName].keys())[1]

    X=Dico[FileName][KeyX]
    Y=Dico[FileName][KeyY]
            
    Extremas=np.array([])
    for i in range(1,len(X)-1):
        diff1_s=(Y[i+1]-Y[i])/(X[i+1]-X[i])
        diff1_p=(Y[i]-Y[i-1])/(X[i]-X[i-1])
        diff2  =(diff1_s-diff1_p)/(X[i+1]-X[i-1])
        if(abs(diff1_p)<Epsilon and diff2<0):
            Extremas=np.append(Extremas,i)
    return Extremas

def Integer(FileName):
    
    KeyX=list(Dico[FileName].keys())[0]
    KeyY=list(Dico[FileName].keys())[1]
    
    X=Dico[FileName][KeyX]
    Y=Dico[FileName][KeyY]
    Y_p=np.zeros(len(X))
    for i in range(1,len(X)):
        Y_p[i]=Y_p[i-1]+Y[i]*(X[i]-X[i-1])

    return Y_p

def LogarithmicDecay(FileName): #INSTABLE
    KeyY=list(Dico[FileName].keys())[1]    
    Y=Integer(FileName)    
    LocalMaxIndex=LocalMax(FileName,0.5)    
    LogDecay=0.0

    j=0
    for i in range(len(LocalMaxIndex)-1):
        if(Y[i]*Y[i+1]>0):
            print(20*log(Y[i]/Y[i+1]))
            LogDecay=LogDecay+20*log(Y[i]/Y[i+1])
            j=j+1
    LogDecay=LogDecay/j
    return LogDecay

#Dans notre cas les colones 0,1 et 5 étaient à conserver
SelectedCol=[0,1,5]

#Mesure des dimensions

for FileName in ls :
    if('.csv' in FileName):
        csvFileList=np.append(csvFileList,FileName)
#        os.system("cp "+Sources+"/"+FileName+" "+Dest)
        N_File=N_File+1
        with open(Sources+"/"+FileName,'r',encoding='utf8') as FileReader:
            FileContent=FileReader.read()
            N_Row=max(N_Row,len(FileContent.split('\n')))
#            N_Col=max(N_Col,len(FileContent.split('\n')[0].split(';')))
                    
N_Row=N_Row-1 #A cause du EOF sans doute...

#Remplissage 
AllData=np.zeros((N_File,N_Row,len(SelectedCol)))

i=0
j=0
k=0

Dico={}
SubDico={}


for FileName in csvFileList:
    with open(Sources+"/"+FileName,'r',encoding='utf8') as FileReader:
        
        #Reduction du fichier        
        with open(Dest+"/"+FileName,'w',encoding='utf8') as FileWriter:
            ReducedContent=SelectCol(FileReader,[0,1,5])
            csvWriter=csv.writer(FileWriter,delimiter=';')
            for l in ReducedContent:
                csvWriter.writerow(l)
            FileWriter.close()
        
    with open(Dest+"/"+FileName,'r',encoding='utf8',newline="") as FileReader:
        #Remplisage
        csvReader=csv.reader(FileReader,delimiter=';')
        Header=next(csvReader)
        j=0
        for Row in csvReader:
            try:
                AllData[i][j]=dfc.replace(Row,',','.')
                j=j+1
            except (ValueError):
                pass
                
            
        k=0
        SubDico={}
        for ColName in Header:
            SubDico[ColName]=AllData[i][:,k]
            k=k+1
        Dico[FileName]=SubDico        
        
        i=i+1
        

plt.ioff() #Empeche d'afficher l'image

       

#En fait toutes les données sont dans Dico de façon très élégantes
    
#Todo: Eviter l'utilisation abusive de mémoire (Tous les tableaux ont la dimension maximale)



